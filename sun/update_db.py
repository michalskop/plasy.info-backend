"""Update DB with today Sun values."""

# see https://stackoverflow.com/questions/54898304/google-firebase-get-update-or-create-documents-using-python
# see https://stackoverflow.com/questions/54900709/python-firebase-update-items-using-where-query/54919991#54919991

import json
import firebase_admin
from firebase_admin import credentials, firestore
import requests

# load settings
with open("settings.json") as fin:
    settings = json.load(fin)

# load data
r = requests.get(settings['data_url'])
if r.status_code == 200:
    data = r.json()

# connect to DB
databaseURL = {
    'databaseURL': settings['databaseURL']
}
cred = credentials.Certificate(settings['certificate'])
firebase_admin.initialize_app(cred, databaseURL)
database = firestore.client()

# update value
col_ref = database.collection(settings['collection'])
results = col_ref.where('name', '==', 'today').get()
for item in results:
    doc = col_ref.document(item.id)
    doc.update(data)
