# Sunrise, sunset values for Plasy

## Calculation of today values
`function.py`
A Python script, which runs as a Google Cloud Function.

Admin page: https://console.cloud.google.com/functions/list?project=plasy-info

Function: https://console.cloud.google.com/functions/details/us-central1/function-sun-1?project=plasy-info&tab=general&duration=PT1H

Deployed: manually (Edit)

Access: https://us-central1-plasy-info.cloudfunctions.net/function-sun-1

Returns JSON object with `sunrise`, `sunset`, `solarnoon`.

All values are in GMT if deployed on Google! (All values are in local timezone.)

## Updating DB
Python script `update_db.py` running on custom server (pi)

Database admin page: https://console.firebase.google.com/project/plasy-info-db/database/firestore/data~2Fsun

### Setup
Install virtual environment
```
sudo pip3 install virtualenv
cd <project-sun>
virtualenv -p python3 env
source env/bin/activate
```
Install required modules
```
pip3 install -r requirements.txt
```
Add `serviceKey.json` file (gitignored) !!!

Possibly deactivate virtualenv
```
deactivate
```
### Running
Manually:
```
cd <project-sun>
source env/bin/activate
python3 ./update_db.py
deactivate
```

### Cron
Add cron entry from `cron.txt`
```
crontab -e
```
