# Lunches in Plasy

## Store the values
A Google sheet.

Link: https://docs.google.com/spreadsheets/d/1McbED7CTNXKbT6wHz3OdnJ9ppQI6hloVjtoVmFb2Yt4/edit#gid=0

Update it every week.

## Updating DB
Python script `update_db.py` running on custom server (pi)

Database admin page: https://console.firebase.google.com/project/plasy-info-db/database/firestore/data~2Flunch

### Setup
Install virtual environment
```
sudo pip3 install virtualenv
cd <project-lunch>
virtualenv -p python3 env
source env/bin/activate
```
Install required modules
```
pip3 install -r requirements.txt
```
Add `serviceKey.json` file (gitignored) !!!

Possibly deactivate virtualenv
```
deactivate
```
### Running
Manually:
```
cd <project-lunch>
source env/bin/activate
python3 ./update_db.py
deactivate
```

### Cron
Add cron entry from `cron.txt`
```
crontab -e
```
