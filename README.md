# Backend for Plasy.info

## Sun
Sunrise and sunset time for current day

See Readme: https://gitlab.com/michalskop/plasy.info-backend/tree/master/sun

## Lunch

See Readme: https://gitlab.com/michalskop/plasy.info-backend/tree/master/lunch
