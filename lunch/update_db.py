"""Update DB with today lunch values."""

import csv
import json
import firebase_admin
from firebase_admin import credentials, firestore
import io
import requests

# load settings
with open("settings.json") as fin:
    settings = json.load(fin)

# load data
r = requests.get(settings['data_url'])
if r.status_code == 200:
    r.encoding = 'utf-8'
    csvio = io.StringIO(r.text, newline="")
    rawdata = []
    for row in csv.DictReader(csvio):
        rawdata.append(row)

# prepare data
dates = {}
lasts = {
    "venue": "",
    "date": ""
}
logicals = ["soup", "vegetarian"]
data = []
for row in rawdata:
    item = row
    # price
    try:
        item['price'] = float(item['price'].strip())
    except Exception:
        item['price'] = 0
    # number
    try:
        item['number'] = int(item['number'].strip())
    except Exception:
        item['number'] = 0
    # logicals
    for k in logicals:
        if item[k].strip() != '':
            item[k] = True
        else:
            item[k] = False
    # possibly missing
    for k in lasts:
        if row[k].strip() == '':
            item[k] = lasts[k]
        else:
            lasts[k] = row[k]
        dates[item['date']] = True
    # sanitize food
    item['food'] = ' '.join(item['food'].capitalize().split())
    # append
    data.append(item)
# dates
dates_arr = []
for k in dates:
    if k != '':
        dates_arr.append(k)

# connect do DB
databaseURL = {
    'databaseURL': settings['databaseURL']
}
cred = credentials.Certificate(settings['certificate'])
firebase_admin.initialize_app(cred, databaseURL)
database = firestore.client()


def _in_data(it, dat):
    # finds if the item is in date
    for row in dat:
        if it['date'] == row['date'] and it['venue'] == row['venue'] and it['food'] == row['food']:
            return True
    return False


# update/insert values
inserts = []
col_ref = database.collection(settings['collection'])
results = col_ref.where('date', '>=', sorted(dates_arr)[0]).where('date', '<=', sorted(dates_arr)[-1]).get()
in_db = []
for item in results:
    in_db.append(item.to_dict())
for item in data:
    if not _in_data(item, in_db):
        inserts.append(item)

batch = database.batch()
for item in inserts:
    doc_ref = col_ref.document()
    batch.set(doc_ref, item)

batch.commit()
